import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import logo from './logo.svg';
import './App.css';
import {models} from './models.js'
import Konva from 'konva';
//import {Stage,Layer,Rect,Label,Tag,Text} from 'react-konva';

class App extends Component {
  state = {
    models:models,
    varFields: '',
    order:"",
    orderData:{
      order:"5548Ch",
      doorColor:"choco",
      doorType:"4K",
      client:"e5",
      tag:"10-407",
      hinge:"reg",
      handleType:"handle",
      orderType:"kitchen",
      dateCut:"2018-10-30",
      dateTFD:"2018-11-02",
      dateAssy:"2018-11-05"
    },
    wall:1,
    ow:0,
    color:false,
    showVars:true,
    staged:[],
    display:[],
    doorsToStage:"",
    skipDr:false,
    item:{type:'',vars: []},
    doorType:"Shaker"
  }



  _idGen = (staged,wall,pos)=>{
    let seq = staged.filter(check=>(check.id[0]===wall.toString())&&(check.id[1]===pos)).length+1;
    seq=(seq<10?"0"+seq:seq);
    return (wall+pos+seq);
  };

  _manageState(e,action){
    if (action==='copy'){
      let xStaged=[];
      this.state.staged.map(x=>{
        let yVars=[];
        x.vars.map(y=>{yVars.push({name:y.name,val:y.val});return null;})
        xStaged.push({id:x.id,vars:yVars,pos:x.pos,type:x.type,doors:x.doors});
        return null;
      })
      let temp = {staged:xStaged,order:this.state.order,wall:this.state.wall};
      document.getElementById("output").value=JSON.stringify(temp);
      document.getElementById("output").select();
      //document.execCommand(action);
    }else if(action==='paste'){
      let temp = JSON.parse(document.getElementById("output").value);
      this.setState({...temp},()=>{
        this._genDisplay();
      });
    }else if(action==='qentry'){
      let temp="";
      this.state.staged.map(item=>{
        temp+=this._genQentry(item);
        document.getElementById("output").value=JSON.stringify(temp).substr(1,temp.length);
        document.getElementById("output").select();
        return null;
      });
    }else if(action==='qentryDr'){
      let temp="";
      let allDoors = [];
      this.state.staged.map(x=>{ //take all doors in cabs on stage
        if(typeof x.doors!=='undefined'){x.doors.map(y=>{allDoors.push(y)})};
        return null;
      });
      allDoors.filter(x=>x.type!=="blank").map(y=>{ // only take doors into consideration
        temp+=(this.state.doorType+"~"+this.state.order+"~"+y.qty+"~"+this._fr(parseFloat(y.w))+"~"+this._fr(parseFloat(y.h))+"~ ~");
      })
      document.getElementById("output").value=JSON.stringify(temp).substr(1,temp.length);
      document.getElementById("output").select();
      return null;
    }else if(action==='doorList'){
      let allDoors = [];
      this.state.staged.map(x=>{ //take all doors in cabs on stage
        if(typeof x.doors!=='undefined'){x.doors.map(y=>{allDoors.push(y)})};});
      allDoors=JSON.parse(JSON.stringify(allDoors.filter(x=>x.type!=="blank"))); // only take doors into consideration
      allDoors.map((x,i)=>{  //go through each door
        allDoors.map((y,j)=>{ //look for others with the same size and type
          if (x.w===y.w&&x.h===y.h&&i!==j){
            x.qty+=y.qty; //add quantities to original
            y.qty=0;
            //allDoors.splice(j,1) //and remove the other <<<glitch pushes modified quantities into state.stage
          }
        });
      });
      allDoors=JSON.parse(JSON.stringify(allDoors.filter(x=>x.qty!==0)));
      allDoors.sort((a,b)=>{return(b.w-a.w)}) // sort by width within heights
      allDoors.sort((a,b)=>{return(b.h-a.h)}) // finally sort by height to get desired order
      let formatted="";
      allDoors.map(x=>{formatted+=(x.qty+"\t"+this._fr(x.w)+"\t"+this._fr(x.h)+"\n");return null;}); // format the array for output
      document.getElementById("output").value=formatted;
      document.getElementById("output").select();
    }
  }

  _editStaged(e,act){
    if (act==="del"){
      let newStaged=[]; //return a filtered and renumbered version of the existing stage, when deleting
      this.state.staged.filter(x=>x.id!==e.target.id).map(x=>newStaged.push({...x,id:this._idGen(newStaged,x.id.substr(0,1),x.id.substr(1,1))}));
      this.setState({staged:newStaged,item:JSON.parse(JSON.stringify({...this.state.item,id:this._idGen(newStaged,this.state.wall,this.state.item.pos)}))},()=>{
        this._genDisplay()}); //update display when done
    }else if(act==="edit"){
      let vars=[];
      let newItem=this.state.staged.filter(x=>x.id===e.target.id)[0]; //take item to be edited
      this.state.staged.filter(x=>x.id===e.target.id)[0].vars.map(x=> //add missing var specs, using appropriate models entry as template
        vars.push({...this.state.models.filter(x=>x.type===newItem.type)[0].vars.filter(y=>y.name===x.name)[0],val:x.val}));
      newItem.presets = this.state.models.filter(x=>x.type===newItem.type)[0].presets; //add missing presets from models also
      newItem.vars=vars;
      this.setState({item:JSON.parse(JSON.stringify(newItem))},()=>{this._setFields(this.state.item)}); //give vars to current item
      //NOTE: all this action does is assign the same id (eg. "1U01") to be applied when submitted. _stageItem handles overwriting mathcing item.
    }else if(act==="add"&&this.state.item.pos===e.target.id.substr(1,1)){
      let newStaged=[]; //add only works with items which match position of clicked item.
      let newItem=this.state.item;
      delete newItem.presets; //take current item without the presets and prepare to push
      this.state.staged.map(x=>{
        if (x.id===e.target.id){
          newStaged.push({...newItem,id:e.target.id}); // when the clicked item is reached, include current...
        }
        newStaged.push({...x,id:this._idGen(newStaged,x.id.substr(0,1),x.id.substr(1,1))}); // ...then renumber the remainder to make sure num gap is filled.
        this.setState({staged:newStaged},()=>{this._genDisplay();this._draw();this._clearType()});
      })
    }else if (act==="add"&&this.state.item.pos!==e.target.id.substr(1,1)){
      console.log("Position (upper/lower/vanity/panel) does not match.");
    }
  }

  _scrolled=(e)=>{}

  _genDisplay=()=>{
    let temp=[[],[],[],[],[]];
    this.state.staged.map(item=>{
      var section='';
      if(item.pos==="U"){
        section=0;
        temp[section].push(item.id+"-"+item.type);
        //temp[section].push(item.id+item.pos+"C"+item.vars.filter(x=>x.name==="h")[0].val+" . . "+
        //  item.vars.filter(x=>x.name==="w")[0].val+"w x "+item.vars.filter(x=>x.name==="d")[0].val+"d ");
      }else if(item.pos==="L"){
        section=1;
        temp[section].push(item.id+"-"+item.type);
        //temp[section].push(item.id+item.type+" . . . "+item.vars.filter(x=>x.name==="w")[0].val+"w ");
      }else if(item.pos==="V"){
        section=2;
        temp[section].push(item.id+"-"+item.type);
        //temp[section].push(item.id+item.type+" . . "+item.vars.filter(x=>x.name==="w")[0].val+"w x "+item.vars.filter(x=>x.name==="d")[0].val+"d ");
      }else if(item.pos==="P"){
        section=3;
        temp[section].push(item.id+"-"+item.type);
        //temp[section].push(item.id+item.type+" . "+item.vars.filter(x=>x.name==="w")[0].val+"w x "+item.vars.filter(x=>x.name==="h")[0].val+"h ");
      }else if(item.pos!=="U"&&item.pos!=="L"&&item.pos!=="V"&&item.pos!=="P"){
        section=4;
        temp[section].push(item.id+item.type+" "+JSON.stringify(item.vars)+" ");
      };
      //return(<button onClick={(e)=>{this._editStaged(e)}} id={item.id} >edit</button>);
      return null;
    })
    let tempFormatted = temp.map(x=>{return(x.map(y=>{return(<li key={y.substr(0,4)}>{y}
      <button onClick={(e)=>{this._editStaged(e,"edit")}} id={y.substr(0,4)} >edit</button>
      <button onClick={(e)=>{this._editStaged(e,"add")}} id={y.substr(0,4)} >addb4</button>
      <button onClick={(e)=>{this._editStaged(e,"del")}} id={y.substr(0,4)} >del</button></li>)}))})
    this.setState({display:tempFormatted});
  }

  _autoDoors=()=>{
    if (typeof this.state.item.doors!=='undefined'){
      let vars={};
      this.state.item.vars.map(x=>{return(vars[x.name]=(x.type==="number"?parseFloat(x.val):x.val))});
      let doors=[];
      if (this.state.item.type==="UC"){
        doors.push({"qty":(vars.w-vars.blankW-.125<23.5?1:2),"w":((vars.w-vars.blankW-.125)<23.5?(vars.w-vars.blankW-(vars.blankW>0?.125:0))-0.125:(vars.w-vars.blankW-(vars.blankW>0?.125:0))/2-0.125),"h":vars.h,"type":"door"});
        if(vars.blankW>0){doors.push({"qty":1,"w":parseFloat(vars.blankW),"h":vars.h,"type":"blank"});};
      }else if (this.state.item.type==="UC_MW"){
        doors.push({"qty":(vars.w-vars.blankW-.125<23.5?1:2),"w":((vars.w-vars.blankW-.125)<23.5?(vars.w-vars.blankW-(vars.blankW>0?.125:0))-0.125:(vars.w-vars.blankW-(vars.blankW>0?.125:0))/2-0.125),"h":vars.doorH,"type":"door"})
      }else if (this.state.item.type==="UC_IKEADR"){
        doors.push({"qty":(vars.w-vars.blankW-.125<23.5?1:2),"w":((vars.w-vars.blankW-.125)<23.5?(vars.w-vars.blankW-(vars.blankW>0?.125:0))-0.125:(vars.w-vars.blankW-(vars.blankW>0?.125:0))/2-0.125),"h":vars.h,"type":"door"});
        if(vars.blankW>0){doors.push({"qty":1,"w":parseFloat(vars.blankW),"h":vars.h,"type":"blank"});};
      }else if (this.state.item.type==="LC"){
        doors.push({"qty":(vars.w-vars.blankW-.125<23.5?1:2),"w":((vars.w-vars.blankW-.125)<23.5?(vars.w-vars.blankW-(vars.blankW>0?.125:0))-0.125:(vars.w-vars.blankW-(vars.blankW>0?.125:0))/2-0.125),"h":vars.h-vars.nh-1,"type":"door"});
        if(vars.blankW>0){doors.push({"qty":1,"w":parseFloat(vars.blankW),"h":vars.h-vars.nh-1,"type":"blank"})};
      }else if (this.state.item.type==="V"){
        doors.push({"qty":(vars.w-vars.blankW-.125<23.5?1:2),"w":((vars.w-vars.blankW-.125)<23.5?(vars.w-vars.blankW-(vars.blankW>0?.125:0))-0.125:(vars.w-vars.blankW-(vars.blankW>0?.125:0))/2-0.125),"h":vars.h-vars.nh-1,"type":"door"});
        if(vars.blankW>0){doors.push({"qty":1,"w":parseFloat(vars.blankW),"h":vars.h-vars.nh-1,"type":"blank"})};
      }else if (this.state.item.type==="VHNS"){
        doors.push({"qty":(vars.w<23.5?1:2),"w":(vars.w<23.5?vars.w-0.125:vars.w/2-0.125),"h":vars.h-vars.nh-1,"type":"door"})
      }else if (this.state.item.type==="VSD"){
        doors.push({"qty":(vars.w<23.5?1:2),"w":(vars.w<23.5?vars.w-0.125:vars.w/2-0.125),"h":vars.h-vars.nh-1,"type":"door"})
      }else if (this.state.item.type==="BOD3V"){
        if (!vars.mbox&&vars.dCt==3){
          doors.push({"qty":3,"w":vars.w-.125,h:Math.round((vars.h-1-vars.nh)/3*4)/4-.125,"type":"door"})
        }else if (!vars.mbox&&vars.dCt==4){
          doors.push({"qty":4,"w":vars.w-.125,h:Math.round((vars.h-1-vars.nh)/4*4)/4-.125,"type":"door"})
        }else if (vars.mbox&&vars.dCt==3){
          doors.push({"qty":1,"w":vars.w-.125,h:6.375,"type":"door"});
          doors.push({"qty":2,"w":vars.w-.125,h:11.625,"type":"door"});
        }else if (vars.mbox&&vars.dCt==4){
          doors.push({"qty":3,"w":vars.w-.125,h:6.25,"type":"door"});
          doors.push({"qty":1,"w":vars.w-.125,h:10.75,"type":"door"});
        }
      }else if (this.state.item.type==="BOD"){
        if (!vars.mbox&&vars.dCt==3){
          doors.push({"qty":3,"w":vars.w-.125,h:Math.round((vars.h-1-vars.nh)/3*4)/4-.125,"type":"door"})
        }else if (!vars.mbox&&vars.dCt==4){
          doors.push({"qty":4,"w":vars.w-.125,h:Math.round((vars.h-1-vars.nh)/4*4)/4-.125,"type":"door"})
        }else if (vars.mbox&&vars.dCt==3){
          doors.push({"qty":1,"w":vars.w-.125,h:6.375,"type":"door"});
          doors.push({"qty":2,"w":vars.w-.125,h:11.625,"type":"door"});
        }else if (vars.mbox&&vars.dCt==4){
          doors.push({"qty":3,"w":vars.w-.125,h:6.25,"type":"door"});
          doors.push({"qty":1,"w":vars.w-.125,h:10.75,"type":"door"});
        }
      }else if (this.state.item.type==="PANTRY"){
        doors.push({"qty":(vars.w<23.5?1:2),"w":((vars.w)<23.5?(vars.w)-0.125:(vars.w)/2-0.125),"h":vars.h-vars.nh-.125,"type":"door"})
      }else if (this.state.item.type==="LC_D"){
        doors.push({"qty":1,"w":vars.w-0.125,"h":vars.h-vars.doorH-vars.nh-1-0.125,"type":"door"});
        doors.push({"qty":(vars.w<23.5?1:2),"w":(vars.w<23.5?vars.w-0.125:vars.w/2-0.125),"h":vars.doorH,"type":"door"});
      }else if (this.state.item.type==="V_D"){
        doors.push({"qty":1,"w":vars.w-0.125,"h":vars.h-vars.doorH-vars.nh-1-0.125,"type":"door"});
        doors.push({"qty":(vars.w<23.5?1:2),"w":(vars.w<23.5?vars.w-0.125:vars.w/2-0.125),"h":vars.doorH,"type":"door"});
      }
      this.setState({item:{...this.state.item,doors:doors}},()=>{this._genDoorsToStage()});
    }
  }

  _genDoorsToStage=()=>{
    let temp=[];
    this.state.item.doors.map(a=>{
      temp.push(a.qty+" x "+a.w+" x "+a.h+(a.type==="blank"?" - blank (Right def)":""));
    })
    temp.push(<button onClick={(e)=>{this._cycleDoors()}} id="cycle" >cycleDoors</button>);
    temp.push(<button onClick={(e)=>{this._blank2dr()}} id="blank2dr" >blank2door</button>);
    temp.push(<button onClick={(e)=>{this.setState({item:{...this.state.item,doors:[]}},()=>this._genDoorsToStage())}} id="delete" >delete</button>);
    this.setState({doorsToStage:temp.map((x,i)=>{return(<li key={"doors"+i}>{x}</li>)})});
  }

  _cycleDoors=()=>{
    let newDoors=[];
    for (let i=this.state.item.doors.length,j=0;i>-1;i--,j++){newDoors[i]=this.state.item.doors[j]}
    this.setState({item:{...this.state.item,doors:newDoors.filter(a=>{return a!==undefined})}},()=>this._genDoorsToStage());
  }

  _blank2dr=()=>{
    if(typeof this.state.item.doors!==undefined){
      let newDoors=this.state.item.doors.map(x=>{return({...x,type:"door"})})
      this.setState({item:{...this.state.item,doors:newDoors.filter(a=>{return a!==undefined})}},()=>this._genDoorsToStage());
    }
  }

  _newType = e => {
    if (typeof models.filter(model => model.type === e.target.value)[0]!=='undefined'){
      let tempType = JSON.parse(JSON.stringify(models.filter(model => model.type === e.target.value)[0]));
      tempType={...tempType,id:this._idGen(this.state.staged,this.state.wall,tempType.pos)};
      this.setState({item:tempType},()=>{this._setFields(tempType);});
    }
  }

  _setPreset=preset=>{
    let tempItem = JSON.parse(JSON.stringify(this.state.item));
    tempItem.vars = JSON.parse(JSON.stringify(models.filter(y=>
      y.type===this.state.item.type)[0].presets.filter(x=>
        x.preset===preset.preset)[0].vars));
    this.setState({item:tempItem},()=>{this._setFields(this.state.item);this._autoDoors()});
  }

  _setFields=item=>{
    let tempVars=JSON.parse(JSON.stringify(item.vars));
    let varFields = tempVars.map(field => {
      if(field.type==="number"){
        return(
          <li key={field.name+'Var'}>
            {field.name}: <input type={field.type} id={field.name} onClick={e=>e.target.select()}
              onChange={e => this._newVal(e,[field.type])} value={field.val}
              min={field.min} max={field.max} step={field.step} style={{width:"60px"}}
              onWheel={(e)=>{e.target.select()}}
              />
          </li>
      )}else if(field.type==="checkbox"){
          return(
            <li key={field.name+'Var'}>
              {field.name}: <input type={field.type} id={field.name}
                onChange={e => this._newVal(e,[field.type])} value={field.val}
                checked={field.val}
                />
            </li>
        )}else if(field.type==="text"){
          return(
            <li key={field.name+'Var'}>
              {field.name}: <input type={field.type} id={field.name}
                onChange={e => this._newVal(e,[field.type])} value={field.val}
                />
            </li>
        )}
        return null;
    });
    let presetFields=(item.presets?item.presets.map(pre=>{
      return(<button key={pre.preset+"btn"} onClick={()=>this._setPreset(pre)} >{pre.preset}</button>)
    }):"")
    this.setState({varFields:varFields,presetFields:presetFields},()=>{
      if (typeof this.state.item.doors!=='undefined'){this._genDoorsToStage()}}
    );
  }

  _stageItem = (item)=>{
    //if (this.state.skipDr){delete item.doors};
    if (this.state.item.vars.length<1){return};
    if (this.state.item.type==='newWall'){
      this.setState({...this.state,wall:this.state.wall++});
    }
    let oldVars = JSON.parse(JSON.stringify(item.vars));
    let tempItem = JSON.parse(JSON.stringify(item));   // create new item to select and edit vars on
    delete item.vars;
    let newVars = oldVars.filter(y=>y.name!=="visL").filter(z=>z.name!=="visR")
      .filter(a=>a.name!=="skipSf").filter(b=>b.name!=="blankW");  // remove vars which need to be reset when staging
    if(typeof models.filter(model => model.type === tempItem.type)[0]!=='undefined'){
      let staging = [...this.state.staged];
      if(typeof staging.filter(x=>x.id===tempItem.id)[0]!=='undefined'){
        staging.splice(staging.indexOf(staging.filter(x=>x.id===tempItem.id)[0]),1,JSON.parse(JSON.stringify(tempItem))); // replace item with same ID in staging, if such exists
      }else{
        staging.push(JSON.parse(JSON.stringify(tempItem))); //add to staging if ID is unique
      }
      if(oldVars.some(x=>x.name==="blankW")){newVars.push({ name: "blankW", val: "0", type: "number", step: 0.125 })};
      if(oldVars.some(x=>x.name==="skipSf")){newVars.push({name:"skipSf",val:false,type:"checkbox"})};
      if(oldVars.some(x=>x.name==="visL")){newVars.push({name:"visL",val:false,type:"checkbox"})};
      if(oldVars.some(x=>x.name==="visR")){newVars.push({name:"visR",val:false,type:"checkbox"})};
      tempItem.vars=newVars;  // give new item the reset values for variables
      for (let i=1;i<=this.state.wall;i++){
        staging.filter(x=>x.id.toString().substr(0,2)==i+"U").forEach(y=>{
          if (y.type==='newWall'){
            let temp = y.id;
            let qw = staging.filter(x2=>x2.id.toString().substr(0,2)==i+"U")[staging.filter(x3=>x3.id.toString().substr(0,2)==i+"U").length-1]
            y.id = staging.filter(x2=>x2.id.toString().substr(0,2)==i+"U")[staging.filter(x3=>x3.id.toString().substr(0,2)==i+"U").length-1].id;
            staging.filter(x4=>x4.id.toString().substr(0,2)==i+"U")[staging.filter(x5=>x5.id.toString().substr(0,2)==i+"U").length-1].id=temp;
          }
        })
      }
      staging=staging.sort((a,b)=>(a.id>b.id)?1:-1);
      this.setState({...this.state,staged:staging,
        item:{...tempItem}},()=>{
          this.setState({...this.state,
            item:{...tempItem,id:this._idGen(this.state.staged,this.state.wall,this.state.item.pos)}
          },()=>{
            this._setFields(tempItem);
            this._genDisplay();
            this._draw();
          });
      });
    };
  }

  _clearType = ()=>{this.setState(Object.assign({},{item:{type:""},varFields:'',presetFields:'',doorsToStage:''}))}

  _newVal = (e,type) => {
    var val = (type=="checkbox" ? (e.target.checked===false?false:true) : e.target.value);
    let tempVal = [...this.state.item.vars];
    let index = tempVal.map(x=>{return(x.name)}).indexOf(e.target.id);
    tempVal[index].val=val;
    this.setState({
      item:{...this.state.item,vars:[...tempVal]}
    },()=>{this._setFields(this.state.item);this._autoDoors();})
  };

  _scrolled = e=> {console.log(e)}

  _genItemList = () => {return models.map(item=>{return <option key={item.type}> {item.type} </option>;})}

  _genWallsList = () => {
    let temp = Array(this.state.wall).fill();
    return temp.map((x,i)=>{return <option key={'wall'+i}> {i+1} </option>});
  }

  _fr=(input)=>{
    input = parseFloat(input);
    var whole = Math.floor(input);
    var fraction = input % whole;
    var output = "";
    output = whole.toString()
    if(fraction){
      var frc = "";
      if (fraction === 0.0625){frc="1/16";}else if (fraction === 0.125){frc="1/8";
    }else if (fraction === 0.1875){frc="3/16";}else if(fraction === 0.25){frc="1/4";
    }else if (fraction === 0.3125){frc="5/16";}else if(fraction === 0.375){frc="3/8";
    }else if (fraction === 0.4375){frc="7/16";}else if(fraction === 0.5){frc="1/2";
    }else if (fraction === 0.5625){frc="9/16";}else if(fraction === 0.625){frc="5/8";
    }else if (fraction === 0.6875){frc="11/16";}else if(fraction === 0.75){frc="3/4";
    }else if (fraction === 0.8125){frc="13/16";}else if(fraction === 0.875){frc="7/8";
    }else if (fraction === 0.9375){frc="15/16";}else{frc=Math.round(fraction*32).toString()+"/32"};
      output+=" "+frc;
      }
    return output;
    }

    _genQentry = (item) => {
      let order=this.state.order;
      let vars={};
      item.vars.map(x=>vars[x.name]=x.val);
      let color=this.state.color;
      let visL=(item.vars.filter(x=>x.name==="visL")[0]?item.vars.filter(x=>x.name==="visL")[0].val:"");
      let visR=(item.vars.filter(x=>x.name==="visR")[0]?item.vars.filter(x=>x.name==="visR")[0].val:"");
      let qentry = "";
      let ord = "";
      if (vars.h>0&&item.type==="UC"){
        if (!color && vars.d===12 && (vars.h === 40 || vars.h === 30 || vars.h === 21 || vars.h === 18 || vars.h === 15 || vars.h === 12)){ord="Stock "+order}else{ord=order;};
        qentry+=("UC_LL~"+ord+(visL?" color":"")+"~1~"+this._fr(vars.d)+"~"+this._fr(vars.h)+"~"+(color ? "lEdgeClr=1," : "lEdge=1,")+"bEdge=1, ~");
        qentry+=("UC_RL~"+ord+(visR?" color":"")+"~1~"+this._fr(vars.d)+"~"+this._fr(vars.h)+"~"+(color ? "lEdgeClr=1," : "lEdge=1,")+"bEdge=1, ~");
        if (!color && vars.d===12 && vars.w<=40 && vars.w === parseInt(vars.w,10)){ord="Stock "+order;}else{ord=order;};
        qentry+=("BTM~"+ord+"~1~"+this._fr(vars.w-1.25)+"~"+this._fr(vars.d)+"~"+(color ? "bEdgeClr=1," : "bEdge=1,")+" ~");
        qentry+=("RECT~"+ord+" top~1~"+this._fr(vars.w-1.25)+"~"+this._fr(vars.d-(27/32))+"~"+(color ? "bEdgeClr=1," : "bEdge=1,")+"bore=1,~");
        if (vars.lockSf>0){ord=order;};
        if (parseFloat(vars.blankW)>0){qentry+=("BLANK~"+ord+(color?" color":"")+"~1~"+this._fr(vars.blankW)+"~"+this._fr(vars.h)+"~ ~");};
        if (vars.h>=20&&vars.h<30){
          qentry+=("RECT~"+ord+" shf~1~"+this._fr(vars.w-1.25-(vars.lockSf>0 ? (3/16) : 0))+"~"+this._fr(vars.d-1)+"~bEdge=1,~");
          }else if (vars.h>=30&&vars.h<40){
          qentry+=("RECT~"+ord+" shf~2~"+this._fr(vars.w-1.25-(vars.lockSf>0 ? (3/16) : 0))+"~"+this._fr(vars.d-1)+"~bEdge=1,~");
          }else if (vars.h>=40){
          qentry+=("RECT~"+ord+" shf~3~"+this._fr(vars.w-1.25-(vars.lockSf>0 ? (3/16) : 0))+"~"+this._fr(vars.d-1)+"~bEdge=1,~");
          };
        };
      if (vars.h>0&&item.type==="UC_IKEADR"){
        ord=order;
        qentry+=("UC_IKEADR_LL~"+ord+"~1~"+this._fr(vars.d)+"~"+this._fr(vars.h)+"~"+(color ? "lEdgeClr=1," : "lEdge=1,")+"bEdge=1, ~");
        qentry+=("UC_IKEADR_RL~"+ord+"~1~"+this._fr(vars.d)+"~"+this._fr(vars.h)+"~"+(color ? "lEdgeClr=1," : "lEdge=1,")+"bEdge=1, ~");
        if (!color && vars.d===12 && vars.w<=40 && vars.w === parseInt(vars.w,10)){ord="Stock "+order;}else{ord=order;};
        qentry+=("BTM~"+ord+"~1~"+this._fr(vars.w-1.25)+"~"+this._fr(vars.d)+"~"+(color ? "bEdgeClr=1," : "bEdge=1,")+" ~");
        qentry+=("RECT~"+ord+" top~1~"+this._fr(vars.w-1.25)+"~"+this._fr(vars.d-(27/32))+"~"+(color ? "bEdgeClr=1," : "bEdge=1,")+"bore=1,~");
        if (vars.lockSf>0){ord=order;};
        if (vars.h>=20&&vars.h<30){
          qentry+=("RECT~"+ord+" shf~1~"+this._fr(vars.w-1.25-(vars.lockSf>0 ? (3/16) : 0))+"~"+this._fr(vars.d-1)+"~bEdge=1,~");
          }else if (vars.h>=30&&vars.h<40){
          qentry+=("RECT~"+ord+" shf~2~"+this._fr(vars.w-1.25-(vars.lockSf>0 ? (3/16) : 0))+"~"+this._fr(vars.d-1)+"~bEdge=1,~");
          }else if (vars.h>=40){
          qentry+=("RECT~"+ord+" shf~3~"+this._fr(vars.w-1.25-(vars.lockSf>0 ? (3/16) : 0))+"~"+this._fr(vars.d-1)+"~bEdge=1,~");
          };
        };
      if (vars.h>0&&item.type==="LC"){
        if (!color && ((vars.d===23.5 && vars.h===35 && vars.sfH===15 && vars.nh===4) || (vars.d===23.5 && vars.h===31 && vars.sfH===11 && vars.nh===0))){ord="Stock "+order;}else{ord=order;};
        qentry+=("LC_LL~"+ord+(visL?" color":"")+"~1~"+this._fr(vars.d)+"~"+this._fr(vars.h)+"~nh="+parseFloat(vars.nh)+",sfH="+parseFloat(vars.sfH)+","+(color ? "lEdgeClr=1," : "lEdge=1,")+"~");
        qentry+=("LC_RL~"+ord+(visR?" color":"")+"~1~"+this._fr(vars.d)+"~"+this._fr(vars.h)+"~nh="+parseFloat(vars.nh)+",sfH="+parseFloat(vars.sfH)+","+(color ? "lEdgeClr=1," : "lEdge=1,")+"~");
        if (!color &&vars.d===23.5 && vars.w<=40 && vars.w === parseInt(vars.w,10)){ord="Stock "+order;}else{ord=order;};
        qentry+=("BTM~"+ord+"~1~"+this._fr(vars.w-1.25)+"~"+this._fr(vars.d)+"~"+(color ? "bEdgeClr=1," : "bEdge=1,")+"bore=1, ~");
        if (vars.w<=40 && vars.w === parseInt(vars.w,10) && !vars.lockSf){ord="Stock "+order;}else{ord=order;};
        if (parseFloat(vars.blankW)>0){qentry+=("BLANK~"+ord+(color?" color":"")+"~1~"+this._fr(vars.blankW)+"~"+this._fr(vars.h-vars.nh-1)+"~ ~");};
        qentry+=("RECT~"+ord+" shf~"+(vars.skipSf===true? "0" : "1")+"~"+this._fr(vars.w-1.25-(vars.lockSf>0 ? (3/16) : 0))+"~"+11+"~bEdge=1, ~");
        if (vars.w>40){
          ord=order;
          qentry+=("RECT~"+ord+" Nr~1~"+this._fr(vars.w-1.25)+"~16~bEdge=1,tEdge=1, ~");
          };
        };
      if (vars.h>0&&item.type==="BOD"){
        if (!color &&((vars.d===23.5 && vars.h===35 && vars.nh===4) || (vars.d===23.5 && vars.h===31 && vars.nh===0))){ord="Stock "+order;}else{ord=order;};
        qentry+=("B_U_LL~"+ord+(visL?" color":"")+"~1~"+this._fr(vars.d)+"~"+this._fr(vars.h)+"~nh="+parseFloat(vars.nh)+","+(color ? "lEdgeClr=1," : "lEdge=1,")+"~");
        qentry+=("B_U_RL~"+ord+(visR?" color":"")+"~1~"+this._fr(vars.d)+"~"+this._fr(vars.h)+"~nh="+parseFloat(vars.nh)+","+(color ? "lEdgeClr=1," : "lEdge=1,")+"~");
        if (!color &&((vars.d===23.5 && vars.w<=40 && vars.w === parseInt(vars.w,10)) || (vars.d===12 && vars.w<=40 && vars.w === parseInt(vars.w,10)))){ord="Stock "+order;}else{ord=order;};
        qentry+=("BTM~"+ord+"~1~"+this._fr(vars.w-1.25)+"~"+this._fr(vars.d)+"~"+(color ? "bEdgeClr=1," : "bEdge=1,")+"bore=1, ~");
        if (vars.mbox!==false && vars.dCt <= 3){
          qentry+=("RECT~"+order+" mbox~2~"+this._fr(vars.w-2.5)+"~25~bEdge=1,~RECT~"+order+" mbox~1~"+this._fr(vars.w-2.5)+"~23 3/4~bEdge=1, ~");
          }else if (vars.mbox!==false && vars.dCt >= 4){
          qentry+=("RECT~"+order+" mbox~1~"+this._fr(vars.w-2.5)+"~25~bEdge=1,~RECT~"+order+" mbox~3~"+this._fr(vars.w-2.5)+"~23 3/4~bEdge=1, ~");
          }else if (vars.mbox===false && vars.dCt <= 3){
          qentry+=("DR_SIDE~"+order+" DrS~3~19 1/2~8 1/8~ ~RECT~"+order+" DrS~1~"+this._fr(vars.w-3.5-(1/16))+"~7 1/8~bEdge=1,tEdge=1 ~DR_SIDE~"+order+" DrS~2~"+this._fr(vars.w-3.5-(1/16))+"~8 1/8~ ~");
          }else if (vars.mbox===false && vars.dCt >= 4){
          qentry+=("DR_SIDE~"+order+" DrS~4~19 1/2~8 1/8~ ~RECT~"+order+" DrS~2~"+this._fr(vars.w-3.5-(1/16))+"~7 1/8~bEdge=1,tEdge=1 ~DR_SIDE~"+order+" DrS~2~"+this._fr(vars.w-3.5-(1/16))+"~8 1/8~ ~");
          };
        }
      if (vars.h>0&&item.type==="BOD3V"){
        ord=order;
        qentry+=("B_U_LL~"+ord+(visL?" color":"")+"~1~"+this._fr(vars.d)+"~"+this._fr(vars.h)+"~nh="+parseFloat(vars.nh)+","+(color ? "lEdgeClr=1," : "lEdge=1,")+"~");
        qentry+=("B_U_RL~"+ord+(visR?" color":"")+"~1~"+this._fr(vars.d)+"~"+this._fr(vars.h)+"~nh="+parseFloat(vars.nh)+","+(color ? "lEdgeClr=1," : "lEdge=1,")+"~");
        qentry+=("BTM~"+ord+"~1~"+this._fr(vars.w-1.25)+"~"+this._fr(vars.d)+"~"+(color ? "bEdgeClr=1," : "bEdge=1,")+"bore=1, ~");
        if (vars.mbox!==false && vars.dCt <= 3){
          qentry+=("RECT~"+ord+" mbox~2~"+this._fr(vars.w-2.5)+"~23~bEdge=1, ~RECT~"+ord+" mbox~1~"+this._fr(vars.w-2.5)+"~21 3/4~bEdge=1, ~");
          }else if (vars.mbox!==false && vars.dCt >= 4){
          qentry+=("RECT~"+ord+" mbox~1~"+this._fr(vars.w-2.5)+"~23~bEdge=1, ~RECT~"+ord+" mbox~3~"+this._fr(vars.w-2.5)+"~21 3/4~bEdge=1, ~");
          }else if (vars.mbox===false && vars.dCt <= 3){
          qentry+=("DR_SIDE~"+order+" DrS~3~19 1/2~8 1/8~ ~RECT~"+order+" DrS~1~"+this._fr(vars.w-3.5)+"~7 1/8~bEdge=1,tEdge=1 ~DR_SIDE~"+order+" DrS~2~"+this._fr(vars.w-3.5-(1/16))+"~8 1/8~ ~");
          }else if (vars.mbox===false && vars.dCt >= 4){
          qentry+=("DR_SIDE~"+order+" DrS~4~19 1/2~8 1/8~ ~RECT~"+order+" DrS~2~"+this._fr(vars.w-3.5)+"~7 1/8~bEdge=1,tEdge=1 ~DR_SIDE~"+order+" DrS~2~"+this._fr(vars.w-3.5-(1/16))+"~8 1/8~ ~");
          };
        }
      if (vars.h>0&&item.type==="V"){
        if (!color&&((vars.d===21&&vars.h===30&&vars.nh===4)||((vars.d===21&&vars.h===31&&vars.nh===0)))){ord="Stock "+order;}else{ord=order;};
        qentry+=("V_LL~"+ord+(visL?" color":"")+"~1~"+this._fr(vars.d)+"~"+this._fr(vars.h)+"~nh="+parseFloat(vars.nh)+","+(color ? "lEdgeClr=1," : "lEdge=1,")+"~");
        qentry+=("V_RL~"+ord+(visR?" color":"")+"~1~"+this._fr(vars.d)+"~"+this._fr(vars.h)+"~nh="+parseFloat(vars.nh)+","+(color ? "lEdgeClr=1," : "lEdge=1,")+"~");
        ord=order;
        qentry+=("RECT~"+ord+" btm~1~"+this._fr(vars.w-1.25)+"~"+this._fr(vars.d)+"~"+(color ? "bEdgeClr=1," : "bEdge=1,")+"bore=1, ~");
        if (vars.w>40){
          qentry+=("RECT~"+ord+" Nr~1~"+this._fr(vars.w-1.25)+"~16~bEdge=1,tEdge=1, ~");
          };
        }
      if (vars.h>0&&item.type==="RECT"){
        ord=order;
        qentry+=("RECT~"+ord+(color?" color":"")+"~1~"+this._fr(vars.w)+"~"+this._fr(vars.h)+"~");
        if (vars.wEdge===1){
          qentry+=(color ? "bEdgeClr=1," : "bEdge=1,")
          }else if(vars.wEdge>=2){
          qentry+=(color ? "bEdgeClr=1,tEdgeClr=1, " : "bEdge=1,tEdge=1,")
          };
        if(vars.hEdge===1){
          qentry+=(color ? "lEdgeClr=1," : "lEdge=1,");
          }else if (vars.hEdge>=2){
          qentry+=(color ? "lEdgeClr=1,rEdgeClr=1, " : "lEdge=1,rEdge=1,");
          };
        qentry+=" ~"
        };
      if (vars.h>0&&item.type==="BLANK"){
        if (!color&&((vars.w===11.875 || vars.w===21.875) && vars.h === 30)){ord="Stock "+order;}else{ord=order;};
        qentry+=("BLANK~"+ord+(color?" color":"")+"~1~"+this._fr(vars.w)+"~"+this._fr(vars.h)+"~"+(color ? "bEdgeClr=1,tEdgeClr=1,lEdgeClr=1, " : "bEdge=1,tEdge=1,lEdge=1, "));
      qentry+=" ~"
        }
      if (vars.h>0&&item.type==="NOTCH"){
        ord=order;
        qentry+=("NOTCH_BR~"+ord+(color?" color":"")+"~1~"+this._fr(vars.w)+"~"+this._fr(vars.h)+"~ ~");
        }
      if (vars.h>0&&item.type==="PANTRY"){
        ord=order;
        qentry+=("PANTRY_LL~"+ord+(visL?" color":"")+"~1~"+this._fr(vars.d)+"~"+this._fr(vars.h)+"~"+(color ? "lEdgeClr=1," : "lEdge=1,")+"nh="+parseFloat(vars.nh)+",midSfH="+parseFloat(vars.midSfH)+",doorIO="+(vars.doorIO?1:0)+",dadoIO="+(vars.dadoIO?1:0)+",sf="+parseFloat(vars.sf)+",lockSf="+(vars.lockSf?1:0)+", ~");
        qentry+=("PANTRY_RL~"+ord+(visR?" color":"")+"~1~"+this._fr(vars.d)+"~"+this._fr(vars.h)+"~"+(color ? "lEdgeClr=1," : "lEdge=1,")+"nh="+parseFloat(vars.nh)+",midSfH="+parseFloat(vars.midSfH)+",doorIO="+(vars.doorIO?1:0)+",dadoIO="+(vars.dadoIO?1:0)+",sf="+parseFloat(vars.sf)+",lockSf="+(vars.lockSf?1:0)+", ~");
        if (!color && ((vars.d === 12 || vars.d === 23.5) && vars.w<=40 && vars.w === parseInt(vars.w,10))){ord="Stock "+order;}else{ord=order;};
        qentry+=("BTM~"+ord+"~1~"+this._fr(vars.w-1.25)+"~"+this._fr(vars.d)+"~"+(color ? "bEdgeClr=1," : "bEdge=1,")+" ~");
        if (!color && vars.d===12 && vars.w<=40 && vars.w === parseInt(vars.w,10)){ord="Stock "+order;}else{ord=order;};
        qentry+=("RECT~"+ord+" top~1~"+this._fr(vars.w-1.25)+"~"+this._fr(vars.d-(27/32))+"~"+(color ? "bEdgeClr=1," : "bEdge=1,")+"bore=1, ~");
        if (vars.midSfH>0){
          qentry+=("RECT~"+ord+" top~1~"+this._fr(vars.w-1.25)+"~"+this._fr(vars.d-(27/32))+"~bEdge=1,bore=1, ~");
          }
        if (vars.lockSf>0){ord=order;}
        if (vars.h>=15&&vars.h<30){
          qentry+=("RECT~"+ord+" shf~"+(vars.midSfH>0 ? 0 : 1)+"~"+this._fr(vars.w-1.25-(vars.lockSf>0 ? (3/16) : 0))+"~"+this._fr(vars.d-1)+"~bEdge=1,~");
          }else if (vars.h>=30&&vars.h<45){
          qentry+=("RECT~"+ord+" shf~"+(vars.midSfH>0 ? 1 : 2)+"~"+this._fr(vars.w-1.25-(vars.lockSf>0 ? (3/16) : 0))+"~"+this._fr(vars.d-1)+"~bEdge=1,~");
          }else if (vars.h>=45&&vars.h<60){
          qentry+=("RECT~"+ord+" shf~"+(vars.midSfH>0 ? 2 : 3)+"~"+this._fr(vars.w-1.25-(vars.lockSf>0 ? (3/16) : 0))+"~"+this._fr(vars.d-1)+"~bEdge=1,~");
          }else if (vars.h>=60){
          qentry+=("RECT~"+ord+" shf~"+(vars.midSfH>0 ? 3 : 4)+"~"+this._fr(vars.w-1.25-(vars.lockSf>0 ? (3/16) : 0))+"~"+this._fr(vars.d-1)+"~bEdge=1,~");
          };
        }
      if (vars.h>0&&item.type==="LC_D"){
        ord=order;
        qentry+=("LC_D_LL~"+ord+(visL?" color":"")+"~1~"+this._fr(vars.d)+"~"+this._fr(vars.h)+"~nh="+parseFloat(vars.nh)+","+(color ? "lEdgeClr=1," : "lEdge=1,")+"doorH="+parseFloat(vars.doorH)+"~");
        qentry+=("LC_D_RL~"+ord+(visR?" color":"")+"~1~"+this._fr(vars.d)+"~"+this._fr(vars.h)+"~nh="+parseFloat(vars.nh)+","+(color ? "lEdgeClr=1," : "lEdge=1,")+"doorH="+parseFloat(vars.doorH)+"~");
        qentry+=("BTM~"+ord+"~1~"+this._fr(vars.w-1.25)+"~"+this._fr(vars.d)+"~"+(color ? "bEdgeClr=1," : "bEdge=1,")+"bore=1, ~");
        }
      if (vars.h>0&&item.type==="V_D"){
        ord=order;
        qentry+=("V_D_LL~"+ord+(visL?" color":"")+"~1~"+this._fr(vars.d)+"~"+this._fr(vars.h)+"~nh="+parseFloat(vars.nh)+","+(color ? "lEdgeClr=1," : "lEdge=1,")+"doorH="+parseFloat(vars.doorH)+"~");
        qentry+=("V_D_RL~"+ord+(visR?" color":"")+"~1~"+this._fr(vars.d)+"~"+this._fr(vars.h)+"~nh="+parseFloat(vars.nh)+","+(color ? "lEdgeClr=1," : "lEdge=1,")+"doorH="+parseFloat(vars.doorH)+"~");
        qentry+=("RECT~"+ord+"~1~"+this._fr(vars.w-1.25)+"~"+this._fr(vars.d)+"~"+(color ? "bEdgeClr=1," : "bEdge=1,")+"bore=1, ~");
        }
      if (vars.h>0&&qentry===""){
        //qentry+=("!!!  Add "+item.type+" to cut  !!! with vars "+vars);
        }
      return(qentry);
      }

    _draw=()=>{
      var c = document.getElementById('canvas');
      var ctx = c.getContext('2d');
      ctx.fillStyle="#FFFFFF";
      ctx.fillRect(0, 0, c.width, c.height);  //set background color
      ctx.save();
      let lHeightLabeled=false;
      let staged=[];
      let row=0;
      this.state.staged.map(x=>{
        let vars={};
        x.vars.map(y=>{return(vars[y.name]=y.val)}) //remap vars from array to key-val pairs
        if (x.pos==="U"||x.pos==="L"||x.pos==="V"){staged.push({...x,vars:vars})} //add only cabs from desired wall to draw
      });
      let ow=0;
      for (let i=0;i<this.state.wall;i++){ // calculate overall width, add up all walls unless that makes the drawing width over 260
        ow=Math.max(ow,staged.filter(x=>x.id.substr(0,1)==(i+1)&&x.pos==="U").reduce((a,b)=>{return a+parseFloat(b.vars.w)},0)+(this.state.wall>i+1?2:0)+(ow<260?ow:0),
          staged.filter(x=>x.id.substr(0,1)==(i+1)&&(x.pos==="L"||x.pos==="V")).reduce((a,b)=>{return a+parseFloat(b.vars.w)},0)+(this.state.wall>i+1?2:0)+(ow<260?ow:0));
      }
      c.width = window.innerWidth-80;
      c.height = c.width*7/11;
      let fontSize=c.width/440*(ow<150?10:10*96/ow);
      let u=.65*(ow<150?c.width/110:(c.width*1.3)/ow); //create unit multiplier based on overall width to represent one inch
      let ux=c.width/22,lx=c.width/22; //create holders for running total of upper and lower x-location
      staged.map(cab=>{
        let vars=cab.vars
        let startX=(cab.pos==="U"?ux:(cab.pos==="L"||cab.pos==="V")?lx:0);
        let startY=(cab.pos==="U"?12*u:(cab.pos==="L"||cab.pos==="V")?(100-vars.h)*u:0);
        startY+=row*100*u; // shift down 110" for new row, if wall width exceeded

        if (vars.nh==0){ //draw plastic legs on cabs with notch 0"
          startY-=4*u;
          ctx.fillStyle='black';
          ctx.rect(startX+2.5*u,startY+vars.h*u,2*u,4*u);
          ctx.rect(startX+vars.w*u-4.5*u,startY+vars.h*u,2*u,4*u);
          ctx.stroke();
        }
        ctx.beginPath();
        if(cab.type!=="topGap"&&cab.type!=="btmGap"&&cab.type!=="newWall"){
          ctx.fillStyle='#eeeeee'
          ctx.fillRect(startX,startY,vars.w*u,vars.h*u);  //fill cabinet a light grey shade
          ctx.rect(startX,startY,vars.w*u,vars.h*u);  //create cabinet outline
          ctx.moveTo(startX,startY); //make isometric perspective lines
          ctx.lineTo(startX+3*u,startY-3*u);
          ctx.moveTo(startX+vars.w*u,startY);
          ctx.lineTo(startX+vars.w*u+3*u,startY-3*u);
          ctx.restore();
          ctx.stroke(); //fill the stroke for the box outline
        }else if(cab.type!=="newWall"){
          ctx.font = 1.5*fontSize+"px Verdana";
          ctx.fillStyle = 'black';
          let xPos=(startX+(vars.w-1.5)*u/2)-vars.label.length*fontSize/2.6;
          ctx.fillText(vars.label,xPos,startY+vars.h*u/2) //add label for gap
        }
        ctx.font = fontSize+"px Verdana";
        ctx.fillStyle = 'black';
        if (vars.w>0&&cab.type!=="newWall"){
          ctx.fillText(vars.w,(startX+(vars.w)*u/2)-vars.w.length*fontSize/2.6,
            (cab.pos==="L"||cab.pos==="V"?startY+vars.h*u+4*u+(vars.nh==0?4*u:0):10*u+row*100*u)) // add w dimension
          }
        if (typeof cab.doors!=='undefined'){
          startY+=(cab.pos==="U"||cab.type==="PANTRY"?0:u); // add 1" countertop gap above doors on lower cabinets
          cab.doors.map(door=>{
            for (let i=0;i<parseInt(door.qty);i++){
              ctx.beginPath();
              ctx.fillStyle='#ffffff'
              ctx.fillRect(startX+u/16,startY,door.w*u,door.h*u); // fill doors with color over cabinet shade
              ctx.rect(startX+u/16,startY,door.w*u,door.h*u); // outline doors individually
              ctx.restore();
              if(door.type==="blank"){ // draw X across blank panels
                ctx.moveTo(startX,startY);ctx.lineTo(startX+door.w*u,startY+door.h*u);
                ctx.moveTo(startX,startY+door.h*u);ctx.lineTo(startX+door.w*u,startY);
              }  //insert door styles here
              ctx.stroke();
              if (cab.type==="UC"||cab.type==="LC"||cab.type==="V"||cab.type==="PANTRY"){
                startX+=door.w*u+u/16;
              }else if (cab.type==="BOD"||cab.type==="BOD3V"){
                startY+=door.h*u+u/16;
              }else if (cab.type==="LC_D"||cab.type==="V_D"){
                startX+=(door.qty>1?door.w*u+u/16:0);
                startY+=(door.qty<=1?door.h*u+u/16:0);
              }
            }
          })
          if (cab.type==="UC"){ // add height dimensions to every upper...
            ctx.font = fontSize+"px Verdana";
            ctx.fillStyle = 'black';
            ctx.fillText(vars.h,(startX-2*u)-vars.h.length*fontSize/2.6,startY+vars.h*u/2);
            if(parseFloat(vars.d)!==12){
              ctx.rect(startX-vars.d.length*fontSize/2.6-2.5*u,startY-2*u-fontSize+u,vars.d.length*fontSize,fontSize+.5*u);
              ctx.fillStyle='#FFFBCC'
              ctx.fillRect(startX-vars.d.length*fontSize/2.6-2.5*u,startY-2*u-fontSize+u,vars.d.length*fontSize,fontSize+.5*u);
              ctx.stroke();
              ctx.fillStyle = 'black';
              ctx.fillText("d"+vars.d,startX-vars.d.length*fontSize/2.6-2*u,startY-2*u);
            };
          }else{
            ctx.font = fontSize+"px Verdana";
            ctx.fillStyle = 'black';
            if(parseFloat(vars.d)!==23.5){
              ctx.rect(startX-vars.d.length*fontSize/2.6-2.5*u,startY-2*u-fontSize+u,vars.d.length*fontSize,fontSize+.5*u);
              ctx.fillStyle='#FFFBCC'
              ctx.fillRect(startX-vars.d.length*fontSize/2.6-2.5*u,startY-2*u-fontSize+u,vars.d.length*fontSize,fontSize+.5*u);
              ctx.stroke();
              ctx.fillStyle = 'black';
              ctx.fillText("d"+vars.d,startX-vars.d.length*fontSize/2.6-2*u,startY-2*u);
            };
            if(lHeightLabeled===false){ // ...and the first lower of a wall
            ctx.fillText(vars.h,(startX-vars.w*u-2*u)-vars.h.length*fontSize/2.6,startY+vars.h*u/2);
            lHeightLabeled=!lHeightLabeled;
            }
          }
        }
        if (this.state.showVars===true&&cab.type!=="newWall"){// print vars on cabs when showVars is checked
          ctx.font = fontSize+"px Verdana";
          let line=0;
          for (let x in vars){
            var xLoc=(cab.type!=='topGap'&&cab.type!=='btmGap'&&cab.type!=='BOD'&&cab.type!=='BOD3V'?startX-vars.w*u+u:startX+u)
            var yLoc=(cab.type!=='topGap'&&cab.type!=='btmGap'?startY+2*u+line*fontSize:startY+2*u+line);
            yLoc+=u;
            if (cab.type==='BOD'||cab.type==='BOD3V'){yLoc-=(vars.h-vars.nh-1)*u};
            ctx.fillStyle = 'rgba(255,255,255,.8)';
            ctx.fillRect(xLoc,yLoc-fontSize,
              (x.length+vars[x].toString().length+2)*.4*fontSize,fontSize+.1*u)
            ctx.fillStyle = 'black';
            ctx.fillText(x+":"+vars[x],xLoc,yLoc);
            line++;
          }
          ctx.fillText(cab.id,xLoc,yLoc+fontSize);
        };
        if(cab.pos==="U"){ux+=vars.w*u}else{lx+=vars.w*u};
        if (cab.type==="newWall"){ //restart values offset off end of wall
          let newStart = Math.max(ux,lx)+6*u;
          ctx.moveTo(newStart,startY+80*u); //Draw vertical line for new wall
          ctx.lineTo(newStart,startY+4*u);
          ctx.stroke();
          ux=(newStart>(260*u)?c.width/22:newStart+6*u);
          lx=(newStart>(260*u)?c.width/22:newStart+6*u);
          row+=(newStart>(260*u)?1:0);
        };
      })
      let origin=(c.width/22)/u;
      let wallStart=origin;
      let yStart=6
      for (let i=0;i<this.state.wall;i++){// draw overall width dimensions for each wall
        let wallEnd=Math.max(staged.filter(x=>x.id.substr(0,1)==(i+1)&&x.pos==="U"&&x.type!=="newWall").reduce((a,b)=>{return a+parseFloat(b.vars.w)},0),
          staged.filter(x=>x.id.substr(0,1)==(i+1)&&(x.pos==="L"||x.pos==="V")&&x.type!=="newWall").reduce((a,b)=>{return a+parseFloat(b.vars.w)},0));
        ctx.moveTo(wallStart*u,(yStart+2)*u);
        ctx.lineTo(wallStart*u,yStart*u);
        ctx.lineTo(wallStart*u+wallEnd*u+this.state.ow*u,yStart*u);
        ctx.lineTo(wallStart*u+wallEnd*u+this.state.ow*u,(yStart+2)*u);
        ctx.fillStyle = 'black';
        ctx.stroke();
        ctx.font = fontSize+"px Verdana";
        ctx.fillText((wallEnd?"cabinet space: "+wallEnd:""),(wallEnd+2*wallStart)*u/2-wallEnd.toString().length*fontSize*2,(yStart-1)*u);
        yStart+=(wallStart+wallEnd>260?101:0);
        wallStart=(wallStart+wallEnd>260?origin:wallStart+wallEnd+18);
      }
      this._genForm();
      setTimeout(()=>{this._genForm()},1000);
    }

    _genForm=()=>{
      //return; // disabling Form for production
      let u = (window.innerWidth-80)/11;
      let data=[{x:.5*u,y:0,w:10*u,font:.3*u,name:"Order",val:this.state.order},
        {x:0.5*u,y:.5*u,w:2*u,font:.24*u,name:"Color",val:this.state.orderData.doorColor},
        {x:2.5*u,y:.5*u,w:2*u,font:.24*u,name:"Doors",val:this.state.orderData.doorType},
        {x:4.5*u,y:.5*u,w:2*u,font:.24*u,name:"Client",val:this.state.orderData.client},
        {x:6.5*u,y:.5*u,w:2*u,font:.24*u,name:"Tag",val:this.state.orderData.tag},
        {x:8.5*u,y:.5*u,w:2*u,font:.24*u,name:"Hinge",val:this.state.orderData.hinge},
        {x:0.5*u,y:u,w:2*u,font:.20*u,name:"Knobs/Handles",val:this.state.orderData.handleType},
        {x:2.5*u,y:u,w:2*u,font:.24*u,name:"Type",val:this.state.orderData.orderType},
        {x:4.5*u,y:u,w:2*u,font:.16*u,name:"Cut Date",val:this.state.orderData.dateCut},
        {x:6.5*u,y:u,w:2*u,font:.16*u,name:"TFD Date",val:this.state.orderData.dateTFD},
        {x:8.5*u,y:u,w:2*u,font:.16*u,name:"Assy Date",val:this.state.orderData.dateAssy}];
      let stage = new Konva.Stage({container: 'Form',width: 11*u,height: 8.5*u});
      let layer = new Konva.Layer();
      let outlines={};
      let labels={};
      let i=0;
      for (let key in this.state.orderData){
        outlines[key]=new Konva.Rect({x:data[i].x,y:data[i].y,
          width:data[i].w,height:.5*u,stroke:'black',strokeWidth:.5});
        labels[key]=new Konva.Text({x:data[i].x,y:data[i].y,fill:"#000",
          width:data[i].w,fontSize:data[i].font,fontFamily:'Verdana',
          align:'center',text:data[i].name+": "+data[i].val});
        i++;
        layer.add(outlines[key]);
        layer.add(labels[key]);
      };
      let cvs=new Image(11*u,7.5*u);
      cvs.src=document.getElementById("canvas").toDataURL();
      let img=new Konva.Image({x:0,y:1.5*u,width:11*u,height:7*u,image:cvs,draggable:true});
      layer.add(img);
      stage.add(layer);
      layer.draw();
    }

  render() {//    const { styles } = this

    return (
      <div className='App'>
        <header className="App-header">
          {/*<img src={logo} className="App-logo" alt="logo" />*/}
          <h1 className="App-title">Order Detail {this.state.order}</h1>
        </header>
        <span className='App-intro' >
          <input type='text' id='order' defaultValue={this.state.order} style={{width:"60px"}}
          onChange={e=>this.setState({order:e.target.value})}/>
          <input
            type='text' id='type' list='items'
            onChange={e => this._newType(e)} style={{width:"60px"}}
            value={this.state.item.type}
          />
          <button onClick={()=>this._clearType()} >clear</button> <br />
          <input id="color" type="checkbox" value={this.state.color?true:false}
          checked={this.state.color?true:false}
          onChange={e=>this.setState({color:e.target.checked})}/>not white?<br/>
          {(this.state.varFields.length?" wall: ":'')}
          <select id='wall' list='walls' style={this.state.varFields.length?{}:{display:'none'}}
              onChange={e=>this.setState({item:{...this.state.item,id:this._idGen(this.state.staged,e.target.value,this.state.item.pos)}})}
              value={this.state.varFields.length?this.state.item.id.substr(0,1):''}
          >
            {this._genWallsList()}
          </select>
          {(this.state.varFields.length?" id: "+this.state.item.id:'')}<br />
          {this.state.presetFields}
          <datalist id='items'>
            { this._genItemList() }
          </datalist>
          <form id='variables' >
            { this.state.varFields }
          </form>
          <button onClick={()=>this._stageItem(this.state.item)}>stage</button>
          <button onClick={()=>this._draw()}>draw</button><br/>
          {this.state.doorsToStage}<br/>
          <span id='display'>
            {this.state.display}<br />
          </span>
      </span>
      <span className='App-output'>
        <button onClick={e=>this._manageState(e,"copy")}>export stage</button>
        <button onClick={e=>this._manageState(e,"paste")}>import stage</button>............
        <button onClick={e=>this._manageState(e,"qentry")}>qentry</button>
        <button onClick={e=>this._manageState(e,"qentryDr")}>qentryDr</button>.............
        <button onClick={e=>this._manageState(e,"doorList")}>doorList</button>
        <button onClick={e=>document.getElementById("output").value=''}>clear</button><br/>
        <textarea id='output' value={this.state.output} rows="10" cols="60"></textarea><br/>
        ow: <input type='text' id='ow' defaultValue={this.state.ow} style={{width:"60px"}}
        onChange={e=>this.setState({ow:e.target.value})}/><br/>
        <input id="showVars" type="checkbox" value={this.state.showVars?true:false}
        checked={this.state.showVars?true:false}
        onChange={e=>this.setState({showVars:e.target.checked})}/>show vars?<br/>
      </span>
      <div className='Canvas'>
        <canvas id="canvas" width="300" height="300"></canvas>
      </div><br/>
      <div id='formDetails'>
        Color: <input type='text' id='ordColor' defaultValue={this.state.orderData.doorColor} style={{width:"60px"}}
        onChange={e=>this.setState({...this.state,orderData:{...this.state.orderData,doorColor:e.target.value}})}/>
        Door: <input type='text' id='ordDoorType' defaultValue={this.state.orderData.doorType} style={{width:"60px"}}
        onChange={e=>this.setState({...this.state,orderData:{...this.state.orderData,doorType:e.target.value}})}/>
        Client: <input type='text' id='ordClient' defaultValue={this.state.orderData.client} style={{width:"60px"}}
        onChange={e=>this.setState({...this.state,orderData:{...this.state.orderData,client:e.target.value}})}/>
        Tag: <input type='text' id='ordTag' defaultValue={this.state.orderData.tag} style={{width:"60px"}}
        onChange={e=>this.setState({...this.state,orderData:{...this.state.orderData,tag:e.target.value}})}/>
        Hinge: <input type='text' id='ordHinge' defaultValue={this.state.orderData.hinge} style={{width:"60px"}}
        onChange={e=>this.setState({...this.state,orderData:{...this.state.orderData,hinge:e.target.value}})}/>
        Knobs/Handles: <input type='text' id='ordHandle' defaultValue={this.state.orderData.handleType} style={{width:"60px"}}
        onChange={e=>this.setState({...this.state,orderData:{...this.state.orderData,handleType:e.target.value}})}/><br/>
        Type: <input type='text' id='ordType' defaultValue={this.state.orderData.orderType} style={{width:"60px"}}
        onChange={e=>this.setState({...this.state,orderData:{...this.state.orderData,orderType:e.target.value}})}/>
        Cut Date: <input type='text' id='ordCut' defaultValue={this.state.orderData.dateCut} style={{width:"60px"}}
        onChange={e=>this.setState({...this.state,orderData:{...this.state.orderData,dateCut:e.target.value}})}/>
        TFD Date: <input type='text' id='ordTFD' defaultValue={this.state.orderData.dateTFD} style={{width:"60px"}}
        onChange={e=>this.setState({...this.state,orderData:{...this.state.orderData,dateTFD:e.target.value}})}/>
        Assy Date: <input type='text' id='ordAssy' defaultValue={this.state.orderData.dateAssy} style={{width:"60px"}}
        onChange={e=>this.setState({...this.state,orderData:{...this.state.orderData,dateAssy:e.target.value}})}/>
        <button onClick={()=>this._genForm()}>genForm</button> <br />
      </div>
      <div id='Form'>
      </div><br/>
      </div>
    )
  }
}


ReactDOM.render (
  <App name='orderEntry' />,
  document.getElementById('root')
)

export default App;
