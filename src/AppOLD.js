import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import logo from './logo.svg';
import './App.css';

class App extends Component {
  constructor(){
  super();

  this.newVal=e=>this.setState({[e.target.id]:e.target.value});

  this.listVars=()=>{
    this.state.varFields="";
    this.state.hit={};
    this.state.vars=[];
    if (typeof this.state.models.filter(mdl=>mdl.item===this.state.itemName)[0]!=='undefined'){
      var i = -1;
      this.state.hit=this.state.models.filter(mdl=>mdl.item===this.state.itemName);
      this.state.hit[0].vars.map((one)=>{
          this.state.vars.push({name:one.name,val:one.val});
      })
      this.state.varFields = this.state.hit[0].vars.map(field=>{
        i++;
        return(
          <li key={this.state.vars[i].name+"Var"}>
            {this.state.vars[i].name}: <input type="number" id={this.state.vars[i].name} onChange={e=>this.newVal(e)} defaultValue={this.state.vars[i].val}  />
          <br />
          </li>
        );
      })
    };
  }

  this.state={
      itemName:"",
      models:[
        {"item":"Other","pos":"O","vars":[{"name":"text","val":""}],"presets":[]},
        {"item":"RECT","pos":"P","vars":[{"name":"w","val":"24"},{"name":"wEdge","val":"0"},{"name":"h","val":"35"},{"name":"hEdge","val":"0"}],
          "presets":[
            {"preset":"24x35","vars":[{"name":"w","val":"24"},{"name":"wEdge","val":"0"},{"name":"h","val":"35"},{"name":"hEdge","val":"1"}]},
            {"preset":"24x96","vars":[{"name":"w","val":"24"},{"name":"wEdge","val":"0"},{"name":"h","val":"96"},{"name":"hEdge","val":"1"}]},
            {"preset":"48x48","vars":[{"name":"w","val":"48"},{"name":"wEdge","val":"0"},{"name":"h","val":"48"},{"name":"hEdge","val":"2"}]}]},
        {"item":"NOTCH","pos":"P","vars":[{"name":"w","val":"23.5"},{"name":"h","val":"35"}],
          "presets":[
            {"preset":"23.5 deep","vars":[{"name":"w","val":"23.5"},{"name":"h","val":"35"}]},
            {"preset":"24 deep","vars":[{"name":"w","val":"24"},{"name":"h","val":"35"}]}]},
        {"item":"BLANK","pos":"P","vars":[{"name":"w","val":"21.875"},{"name":"wEdge","val":"2"},{"name":"h","val":"30"},{"name":"hEdge","val":"1"}],
          "presets":[
            {"preset":"LC_Blank","vars":[{"name":"w","val":"21.875"},{"name":"wEdge","val":"2"},{"name":"h","val":"30"},{"name":"hEdge","val":"1"}]},
            {"preset":"UC_Blank","vars":[{"name":"w","val":"11.875"},{"name":"wEdge","val":"2"},{"name":"h","val":"30"},{"name":"hEdge","val":"1"}]}]},
        {"item":"UC","pos":"U","vars":[{"name":"w","val":"30"},{"name":"h","val":"30"},{"name":"d","val":"12"},{"name":"lockSf","val":"0"}],
          "presets":[
            {"preset":"UC30","vars":[{"name":"w","val":"30"},{"name":"h","val":"30"},{"name":"d","val":"12"},{"name":"lockSf","val":"0"}]},
            {"preset":"UC40","vars":[{"name":"w","val":"30"},{"name":"h","val":"40"},{"name":"d","val":"12"},{"name":"lockSf","val":"1"}]}]},
            {"item":"UC_IKEADR","pos":"U","vars":[{"name":"w","val":"30"},{"name":"h","val":"40"},{"name":"d","val":"12"},{"name":"lockSf","val":"0"}],
              "presets":[
                {"preset":"UC_IKEADR","vars":[{"name":"w","val":"30"},{"name":"h","val":"40"},{"name":"d","val":"12"},{"name":"lockSf","val":"0"}]}]},
            {"item":"UC_MW","pos":"U","vars":[{"name":"w","val":"30"},{"name":"h","val":"40"},{"name":"d","val":"12"},{"name":"doorH","val":"15"}],
              "presets":[{"preset":"UC_MW","vars":[{"name":"w","val":"30"},{"name":"h","val":"40"},{"name":"d","val":"12"},{"name":"doorH","val":"15"}]}]},
            {"item":"LC","pos":"L","vars":[{"name":"w","val":"30"},{"name":"h","val":"35"},{"name":"d","val":"23.5"},
                {"name":"sfH","val":"15"},{"name":"nh","val":"4"},{"name":"lockSf","val":"0"},{"name":"skipSf","val":"0"}],
              "presets":[
                {"preset":"LC","vars":[{"name":"w","val":"30"},{"name":"h","val":"35"},{"name":"d","val":"23.5"},
                  {"name":"sfH","val":"15"},{"name":"nh","val":"4"},{"name":"lockSf","val":"0"},{"name":"skipSf","val":"0"}]},
                {"preset":"LC3","vars":[{"name":"w","val":"30"},{"name":"h","val":"31"},{"name":"d","val":"23.5"},
                  {"name":"sfH","val":"11"},{"name":"nh","val":"0"},{"name":"lockSf","val":"1"},{"name":"skipSf","val":"0"}]},
                {"preset":"Sink","vars":[{"name":"w","val":"30"},{"name":"h","val":"35"},{"name":"d","val":"23.5"},
                  {"name":"sfH","val":"15"},{"name":"nh","val":"4"},{"name":"lockSf","val":"0"},{"name":"skipSf","val":"1"}]},
                {"preset":"Sink3","vars":[{"name":"w","val":"30"},{"name":"h","val":"31"},{"name":"d","val":"23.5"},
                  {"name":"sfH","val":"11"},{"name":"nh","val":"0"},{"name":"lockSf","val":"1"},{"name":"skipSf","val":"1"}]}]},
            {"item":"V","pos":"V","vars":[{"name":"w","val":"24"},{"name":"h","val":"30"},{"name":"d","val":"21"},{"name":"nh","val":"4"}],
              "presets":[
                {"preset":"V","vars":[{"name":"w","val":"24"},{"name":"h","val":"30"},{"name":"d","val":"21"},{"name":"nh","val":"4"}]},
                {"preset":"V3","vars":[{"name":"w","val":"24"},{"name":"h","val":"31"},{"name":"d","val":"21"},{"name":"nh","val":"0"}]}]},
            {"item":"VHNS","pos":"V","vars":[{"name":"w","val":"29"},{"name":"h","val":"30"},{"name":"d","val":"21"}],
              "presets":[{"preset":"VHNS","vars":[{"name":"w","val":"29"},{"name":"h","val":"30"},{"name":"d","val":"21"}]}]},
            {"item":"VSD","pos":"V","vars":[{"name":"w","val":"24"},{"name":"h","val":"30"},{"name":"d","val":"18"}],
              "presets":[{"preset":"VSD","vars":[{"name":"w","val":"24"},{"name":"h","val":"30"},{"name":"d","val":"18"}]}]},
            {"item":"BOD3V","pos":"V","vars":[{"name":"w","val":"18"},{"name":"h","val":"31"},{"name":"d","val":"21"},
                {"name":"nh","val":"0"},{"name":"dCt","val":"4"},{"name":"mbox","val":"1"}],
              "presets":[
                {"preset":"BOD3V","vars":[{"name":"w","val":"18"},{"name":"h","val":"31"},{"name":"d","val":"21"},
                  {"name":"nh","val":"0"},{"name":"dCt","val":"4"},{"name":"mbox","val":"1"}]}]},
            {"item":"BOD","pos":"L","vars":[{"name":"w","val":"18"},{"name":"h","val":"35"},{"name":"d","val":"23.5"},
                {"name":"nh","val":"4"},{"name":"dCt","val":"4"},{"name":"mbox","val":"0"}],
              "presets":[
                {"preset":"BOD","vars":[{"name":"w","val":"18"},{"name":"h","val":"35"},{"name":"d","val":"23.5"},{"name":"nh","val":"4"},
                  {"name":"dCt","val":"4"},{"name":"mbox","val":"0"}]},
                {"preset":"BOD3","vars":[{"name":"w","val":"18"},{"name":"h","val":"31"},{"name":"d","val":"23.5"},{"name":"nh","val":"0"},
                  {"name":"dCt","val":"4"},{"name":"mbox","val":"1"}]}]},
            {"item":"PANTRY","pos":"L","vars":[{"name":"w","val":"20"},{"name":"h","val":"80"},{"name":"d","val":"12"},{"name":"nw","val":"3"},
                {"name":"nh","val":"4"},{"name":"sf","val":"4"},{"name":"midSfH","val":"42"},{"name":"doorIO","val":"1"},{"name":"dadoIO","val":"1"},
                {"name":"lockSf","val":"0"}],
              "presets":[
                {"preset":"PANTRY","vars":[{"name":"w","val":"20"},{"name":"h","val":"80"},{"name":"d","val":"12"},{"name":"nw","val":"3"},
                  {"name":"nh","val":"4"},{"name":"sf","val":"4"},{"name":"midSfH","val":"42"},{"name":"doorIO","val":"1"},{"name":"dadoIO","val":"1"},
                  {"name":"lockSf","val":"0"}]},
                {"preset":"ORGANIZER","vars":[{"name":"w","val":"15"},{"name":"h","val":"55"},{"name":"d","val":"10"},{"name":"nw","val":"3"},
                  {"name":"nh","val":"0"},{"name":"sf","val":"2"},{"name":"midSfH","val":"27.5"},{"name":"doorIO","val":"1"},{"name":"dadoIO","val":"1"},
                  {"name":"lockSf","val":"1"}]},
                {"preset":"IKEADRWR","vars":[{"name":"w","val":"20"},{"name":"h","val":"80"},{"name":"d","val":"12"},{"name":"nw","val":"3"},
                  {"name":"nh","val":"4"},{"name":"sf","val":"5"},{"name":"midSfH","val":"42"},{"name":"doorIO","val":"1"},{"name":"dadoIO","val":"1"},
                  {"name":"lockSf","val":"0"}]}]},
            {"item":"LC_D","pos":"L","vars":[{"name":"w","val":"24"},{"name":"h","val":"31"},{"name":"d","val":"23.5"},{"name":"doorH","val":"23.625"},
                {"name":"nh","val":"0"}],
              "presets":[
                {"preset":"LC_D","vars":[{"name":"w","val":"24"},{"name":"h","val":"31"},{"name":"d","val":"23.5"},
                  {"name":"doorH","val":"23.625"},{"name":"nh","val":"0"}]}]},
            {"item":"V_D","pos":"V","vars":[{"name":"w","val":"24"},{"name":"h","val":"31"},{"name":"d","val":"21"},
                {"name":"doorH","val":"23.625"},{"name":"nh","val":"0"}],
              "presets":[
                {"preset":"LC_D","vars":[{"name":"w","val":"24"},{"name":"h","val":"31"},{"name":"d","val":"21"},
                  {"name":"doorH","val":"23.625"},{"name":"nh","val":"0"}]}]}
        ]
      };
    }

  render(){
    this.listVars();
    return (<div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <h1 className="App-title">Parts to Cut</h1>
      </header>
      <p className="App-intro">
        <input type="text" id="itemName"
        onChange={e=>this.newVal(e)} value={this.state.itemName}/><br />
        {this.state.itemName}<br />
        </p>
        <form id="variables">
          {this.state.varFields}
        </form>
     </div>
     );
  }
  // END OF CONSTRUCTOR
}

ReactDOM.render(
  <App name="orderEntry" />,
  document.getElementById('root')
);

export default App;
